//
//  Timbrature+CoreDataProperties.swift
//  myTime
//
//  Created by Luigi Cerrato on 05/12/2019.
//  Copyright © 2019 Luigi Cerrato. All rights reserved.
//
//

import Foundation
import CoreData


extension Timbrature {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Timbrature> {
        return NSFetchRequest<Timbrature>(entityName: "Timbrature")
    }

    @NSManaged public var data: Date?
    @NSManaged public var typeOfTimbrature: String?
//    @NSManaged public var time: String?

}
