//
//  SecondViewController.swift
//  myTime
//
//  Created by Luigi Cerrato on 05/12/2019.
//  Copyright © 2019 Luigi Cerrato. All rights reserved.
//

import UIKit
import FSCalendar



class CalendarViewController: UIViewController {
//    fileprivate weak var calendar: FSCalendar!


    @IBOutlet weak var calendar: FSCalendar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Code to init calendar by code
        
//        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: 320, height: 300))
//
//        //        calendar.dataSource = self
//        //        calendar.delegate = self
//        calendar.register(FSCalendarCell.self, forCellReuseIdentifier: "CELL")
//        view.addSubview(calendar)
//      self.calendar = calendar
        setupCalendar()
        calendar.delegate = self
      
        
    }
    func setupCalendar (){
//        set firsDAy of week at monday
        
      calendar.firstWeekday = 2
        calendar.translatesAutoresizingMaskIntoConstraints = false
        calendar.centerYAnchor .constraint(equalTo: view.centerYAnchor).isActive = true
        calendar.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        calendar.heightAnchor.constraint(equalToConstant: 275).isActive = true
        calendar.widthAnchor.constraint(equalToConstant: view.frame.width - 40).isActive = true
    }
    
    
}
extension CalendarViewController : FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance{
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        "111"
    }
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        "222"
    }
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "CELL", for: date, at: position)
        
        return cell
    }

    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
//        change fill color for a specific date
        
        return .green
    }

    
    
}

