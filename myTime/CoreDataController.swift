//
//  CoreDataController.swift
//  myTime
//
//  Created by Luigi Cerrato on 05/12/2019.
//  Copyright © 2019 Luigi Cerrato. All rights reserved.
//


import Foundation
import UIKit
import CoreData

//Per evitare la ripetizione di codice inutile preferisco crearmi una classe, che generalmente chiamo CoreDataController, dentro la quale inserisco le funzioni che utilizzerò.

class CoreDataController {
    static let shared = CoreDataController()
    
    private var context: NSManagedObjectContext
    private init() {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext 
        
    }
    
    func addToEntity (data : Date,typeOfTimbrature : String){
        let entity = NSEntityDescription.entity(forEntityName: "Timbrature", in: self.context)
        
        
        let newItem = Timbrature(entity : entity! , insertInto : self.context)
        newItem.data = data
        newItem.typeOfTimbrature = typeOfTimbrature
        
        
        do {
            try self.context.save()
        } catch let errore {
            print("[addToEntity] Problem while saving Item with date and time : \(newItem.data!) and\(newItem.typeOfTimbrature!) in memory")
            print("[addToEntity] Stampo l'errore: \n \(errore) \n")
        }
        
        print("[addToEntity] Item with date and time  : \(newItem.data!) and \(newItem.typeOfTimbrature!) saved in memory in the right way")
        
        
    }
    
    func loadAllItem() {
        print("[loadAllItem] I'm Recovring all Data ")
        
        let fetchRequest: NSFetchRequest<Timbrature> = Timbrature.fetchRequest()
        
        //        The following line of codes count all the items in my entity
        //        do {
        //                   let count = try context.count(for: fetchRequest)
        //                   print(" Il numero degli item è \(count)")
        //               } catch {
        //                   print(error.localizedDescription)
        //               }
        //
        //
        do {
            let array = try self.context.fetch(fetchRequest)
            
            guard array.count > 0 else {print("[loadAllItem] There are no Elements To read"); return}
            
            for x in array {
                let item = x
                print("[loadAllItem] Item With date and time : \(item.data!) and \(item.typeOfTimbrature!) it's just now downloaded ")
                
                
            }
            
        } catch let errore {
            print("[loadAllItem] Problem whith FetchRequest")
            print(" Print Error : \n \(errore) \n")
        }
    }
    
    
    func deleteAllData(entity: String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try  context.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                context.delete(managedObjectData)
                try self.context.save()
            }
        } catch let error as NSError {
            print("[deleteAllData] Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
        print("[deleteAllData] All elements are deleted ")
        
    }
    
    
    func printer (date: Date){
        print("[printer] i'm printer....")
        print("[printer] Date and Time in Printer :  \(date) ")
        
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long

        // get the date time String from the date object
        formatter.string(from: date)
        
        // "10:52:30 PM"
        formatter.timeStyle = .medium
        formatter.dateStyle = .none
        formatter.string(from: date)
       
        
    }
}
