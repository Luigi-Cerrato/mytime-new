//
//  BadgeViewController.swift
//  myTime
//
//  Created by Luigi Cerrato on 05/12/2019.
//  Copyright © 2019 Luigi Cerrato. All rights reserved.
//

import UIKit
import CoreData


class FirstViewController: UIViewController {
    
    var userStatus: UserLogInStatus {
        get {
            ( UIApplication.shared.delegate as! AppDelegate).userStatus
        }
        set(newStatus) {
            ( UIApplication.shared.delegate as! AppDelegate).userStatus = newStatus
        }
    }
    @IBOutlet var badgeView: UIView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var BadgeInButton: UIButton!
    
    @IBOutlet weak var BadgeOutButton: UIButton!
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTime()
        setup()
        loadImage()
        
    }
    
    func loadImage(){
        self.badgeView.backgroundColor = UIColor(patternImage: UIImage(named: "GreyGradient.jpg")!)
        
    }
    func setup(){
        //        setup Button
        BadgeInButton.setTitle("Badge In", for: .normal)
        BadgeOutButton.setTitle("Badge Out" , for: .normal)
        //        setup Labels property
        dateLabel.font = UIFont.boldSystemFont(ofSize: 30.0)
        dateLabel.textColor = .white
        timeLabel.font = UIFont.boldSystemFont(ofSize: 50.0)
        timeLabel.textColor = .white
        
    }
    func getTime(){
        
        dateLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .none)
        timeLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .medium)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
        
    }
    
    @objc func tick() {
        dateLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .none)
        timeLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .medium)
    }
    
    @IBAction func BadgeIn(_ sender: UIButton) {
        
        self.userStatus = .loggedIn
        //        sender.isHidden = true
        //        BadgeOutButton.isHidden = false
        //        Insert Data with CoreDataController
        let current : Date = Date()
        print (current)
        CoreDataController.shared.addToEntity(data: current, typeOfTimbrature: "Badge In")
    }
    
    @IBAction func getAllDAta(_ sender: Any) {
        //    Get All Data From CoreDataController
        CoreDataController.shared.loadAllItem()
    }
    
    @IBAction func deleteAllItem(_ sender: Any){
        CoreDataController.shared.deleteAllData(entity: "Timbrature")
        //        CoreDataController.shared.printer(date: Date())
    }
    
    @IBAction func BadgeOut(_ sender: UIButton) {
        self.userStatus = .loggedOut
        //        BadgeInButton.isHidden = false
        //        sender.isHidden = true
        let current : Date = Date()
        print (current)
        CoreDataController.shared.addToEntity(data: current, typeOfTimbrature: "Badge Out")
    }
    //    Todo implementare linvio dal bottone sender e poi spostarlo su un bottone chck in risolvere data e time
    
}


enum UserLogInStatus {
    case loggedIn
    case loggedOut
    
}





