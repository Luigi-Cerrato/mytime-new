//
//  TestCalendarViewController.swift
//  myTime
//
//  Created by Luigi Cerrato on 10/12/2019.
//  Copyright © 2019 Luigi Cerrato. All rights reserved.
//

import UIKit
import FSCalendar

class TestCalendarViewController: UIViewController,FSCalendarDataSource, FSCalendarDelegate {

    @IBOutlet weak var calendar: FSCalendar!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        calendar.register(FSCalendarCell.self, forCellReuseIdentifier: "CELL")
             
               setupCalendar()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

        func setupCalendar (){
            calendar.translatesAutoresizingMaskIntoConstraints = false
            calendar.centerYAnchor .constraint(equalTo: view.centerYAnchor).isActive = true
            calendar.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            calendar.heightAnchor.constraint(equalToConstant: 275).isActive = true
            calendar.widthAnchor.constraint(equalToConstant: view.frame.width - 40).isActive = true
        }
        
        
    }





